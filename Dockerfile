
FROM registry.centos.org/centos/centos:7

MAINTAINER Karanbir Singh <container-feedback.inb@karan.org>

ENV GITEA_VER=1.2.0-rc1

LABEL License=GPLv2
LABEL Version=${GITEA_VER}

COPY ./root /
RUN useradd -ms /bin/bash git
RUN yum -y install --setopt=tsflags=nodocs git && \
    yum -y upgrade && yum clean all && \
    curl -L -o /tmp/gitea https://dl.gitea.io/gitea/${GITEA_VER}/gitea-${GITEA_VER}-linux-amd64 && \
    mkdir -p /opt/git/ && mv /tmp/gitea /opt/git/ && chmod a+x /opt/git/gitea && \
    chmod 777 /opt/git

EXPOSE 3000

USER 1001

ENTRYPOINT ["/usr/local/bin/rungitea" ]
